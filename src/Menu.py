##
## Menu
##

import pygame

from . import User_interface, Constant

class Menu:

    def __init__(self):
        self.buttons = {}
        self.buttons["QUIT"] = User_interface.Button(Constant.FONT_PATH, [250, 60], [515, 550], 2,
                                                     [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                     {"TXT": "Quit",
                                                      "SIZE": 28,
                                                      "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]})
        self.buttons["HOWTO"] = User_interface.Button(Constant.FONT_PATH, [250, 60], [515, 450], 2,
                                                      [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                      {"TXT": "How To Play",
                                                       "SIZE": 28,
                                                       "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]})
        self.buttons["LEVEL"] = User_interface.Button(Constant.FONT_PATH, [250, 60], [515, 350], 2,
                                                      [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                      {"TXT": "Play",
                                                       "SIZE": 28,
                                                       "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]})
        self.background = pygame.image.load(Constant.ASSETS_DIR + "/menu_back.png")

    def draw(self, screen):
        screen.blit(self.background, [0, 0])
        for i in iter(self.buttons):
            self.buttons[i].draw(screen)

    def update(self):
        for i in iter(self.buttons):
            if self.buttons[i].is_overflew() and pygame.mouse.get_pressed()[0]:
                self.buttons[i].set_state(0)
                pygame.mouse.set_pos([0, 0])
                return (i)
            elif self.buttons[i].is_overflew():
                self.buttons[i].set_state(1)
            else:
                self.buttons[i].set_state(2)
        return ("MENU")
