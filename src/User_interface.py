##
## User interface class definitions
##

import pygame

##
## Base class for interface object
##
class Interface_object(object):

    def __init__(self, path, size, position, state, color, txt):
        self.path = path
        self.size = size
        self.pos = position
        self.state = state
        self.color = color
        self.txt = txt["TXT"]
        self.txt_size = txt["SIZE"]
        self.txt_color = txt["COLOR"]
        self.origin = [0, 0]

    ##
    ## state :
    ## * 0 -> active
    ## * 1 -> overflew
    ## * 2 -> inactive
    ##

    def set_state(self, new_state):
        self.state = new_state

    def get_state(self):
        return self.state

    def set_origin(self, origin):
        self.pos = [self.pos[0] - self.origin[0] + origin[0], self.pos[1] - self.origin[1] + origin[1]]
        self.origin = origin

    def get_pos(self):
        return (self.pos)

    def is_overflew(self):
        if (pygame.mouse.get_pos()[0] > self.pos[0]
            and pygame.mouse.get_pos()[1] > self.pos[1]
            and pygame.mouse.get_pos()[0] < self.pos[0] + self.size[0]
            and pygame.mouse.get_pos()[1] < self.pos[1] + self.size[1]):
            return True
        return False


##
## Button object
##
class Button(Interface_object):

    def __init__(self, path, size, position, state, color, txt):
        super(Button, self).__init__(path, size, position, state, color, txt)

    def draw(self, screen):
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = font.render(self.txt, True, self.txt_color[self.state])
        pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
        screen.blit(text_surface, [self.pos[0] + (self.size[0] - text_surface.get_size()[0]) / 2,
                                   self.pos[1] + (self.size[1] - text_surface.get_size()[1]) / 2])

##
## Drop Menu
##
class Drop_menu(Interface_object):

    def __init__(self, path, size, position, state, color, txt, content_list, active_element):
        super(Drop_menu, self).__init__(path, size, position, state, color, txt)
        self.dropped = False
        self.content_list = content_list
        self.button_list = {}
        y_button = position[1]
        for i in iter(content_list):
            y_button += size[1]
            txt["TXT"] = content_list[i]
            self.button_list[i] = Button(path, size, [position[0], y_button], 2, color, txt, False)
            if i == active_element:
                self.button_list[i].set_state(0)

    def is_dropped(self):
        return self.dropped

    def get_active_element(self):
        for i in iter(self.button_list):
            if self.button_list[i].get_state() == 0:
                return i

    def change_active_element(self):
        if not self.dropped:
            self.dropped = True
            return None
        else:
            self.dropped = False
            for i in iter(self.button_list):
                if self.button_list[i].is_overflew():
                    for j in iter(self.button_list):
                        self.button_list[j].set_state(2)
                    self.button_list[i].set_state(0)
                    return i
        return None

    def is_overflew(self):
        if self.dropped:
            for i in iter(self.button_list):
                if self.button_list[i].is_overflew() and self.button_list[i].get_state() != 0:
                    self.button_list[i].set_state(1)
                elif self.button_list[i].get_state() != 0:
                    self.button_list[i].set_state(2)
        return super(Drop_menu, self).is_overflew()

    def draw(self, screen):
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = font.render(self.txt, True, self.txt_color[0])
        screen.blit(text_surface, [self.pos[0] + (self.size[0] - text_surface.get_size()[0]) / 2,
                                   self.pos[1] - text_surface.get_size()[1] - self.txt_size / 4])
        if not self.dropped:
            for i in iter(self.button_list):
                if self.button_list[i].get_state() == 0:
                    current_txt = self.content_list[i]
            text_surface = font.render(current_txt, True, self.txt_color[self.state])
            pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
            screen.blit(text_surface, [self.pos[0] + (self.size[0] - text_surface.get_size()[0]) / 2,
                                       self.pos[1] + (self.size[1] - text_surface.get_size()[1]) / 2])
        else:
            for i in iter(self.button_list):
                self.button_list[i].draw(screen)
            pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size))
            pygame.draw.aalines(screen, self.txt_color[self.state], True,
                                [[int(self.pos[0] + self.size[0] - self.size[1] / 2),
                                  int(self.pos[1] + self.size[1] * 0.8)],
                                 [self.pos[0] + self.size[0], int(self.pos[1] + self.size[1] * 0.2)],
                                 [self.pos[0] + self.size[0] - self.size[1], int(self.pos[1] + self.size[1] * 0.2)]], True)

##
## Check box object (switch)
##
class Check_box(Interface_object):

    def __init__(self, path, size, position, state, color, txt, tick, width):
        super(Check_box, self).__init__(path, size, position, state, color, txt)
        self.tick = tick
        self.width = width

    def toggle(self):
        if self.state == 0:
            self.state = 1
        else:
            self.state = 0

    def draw(self, screen):
        font = pygame.font.Font(self.path, self.txt_size)
        text_surface = font.render(self.txt, True, self.txt_color)
        screen.blit(text_surface, [int(self.pos[0] + self.size[0] + self.txt_size / 2), self.pos[1]])
        if self.state == 0:
            pygame.draw.aaline(screen, self.tick,
                               [self.pos[0], self.pos[1]],
                               [self.pos[0] + self.size[0],
                                self.pos[1] + self.size[1]], True)
            pygame.draw.aaline(screen, self.tick,
                               [self.pos[0] + self.size[0], self.pos[1]],
                               [self.pos[0], self.pos[1] + self.size[1]], True)
        pygame.draw.rect(screen, self.color[self.state], pygame.Rect(self.pos, self.size), self.width)
