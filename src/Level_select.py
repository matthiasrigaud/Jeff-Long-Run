##
## level selection menu
##

import pygame
import json

from . import User_interface, Constant

class Level_select:

    def __init__(self):
        self.lock = pygame.image.load(Constant.ASSETS_DIR + "/lock.png")
        default_save = {}
        all_level_file = open(Constant.LEVEL_FILE)
        self.all_level_data = json.load(all_level_file)
        all_level_file.close()
        self.buttons = {}
        y = 70
        for i in iter(self.all_level_data["DATA"]):
            for j in iter(i):
                default_save[i[j]] = False
                if len(default_save) == 1:
                    default_save[i[j]] = True
                self.buttons[i[j]] = User_interface.Button(Constant.FONT_PATH, [250, 60], [515, y], 2,
                                                           [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                           {"TXT": j,
                                                            "SIZE": 28,
                                                            "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]})
                y += 100
        self.buttons["MENU"] = User_interface.Button(Constant.FONT_PATH, [350, 80], [20, 620], 2,
                                                     [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                     {"TXT": "<< Back to Menu",
                                                      "SIZE": 28,
                                                      "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]})
        try:
            save_file = open(Constant.SAVE_FILE, "r+")
            try:
                self.save = json.load(save_file)
            except ValueError:
                self.save = default_save
                save_file.close()
                save_file = open(Constant.SAVE_FILE, "w+")
                json.dump(default_save, save_file)
                save_file.close()
        except IOError:
            self.save = default_save
            save_file = open(Constant.SAVE_FILE, "w+")
            json.dump(default_save, save_file)
            save_file.close()

    def draw(self, screen):
        screen.fill([157, 180, 195])
        for i in iter(self.buttons):
            self.buttons[i].draw(screen)
            if i in self.save and not self.save[i]:
                screen.blit(self.lock, self.buttons[i].get_pos())

    def update(self):
        for i in iter(self.buttons):
            if self.buttons[i].is_overflew() and pygame.mouse.get_pressed()[0] and (not i in self.save or self.save[i]):
                self.buttons[i].set_state(0)
                return (i)
            elif self.buttons[i].is_overflew():
                self.buttons[i].set_state(1)
            else:
                self.buttons[i].set_state(2)
        return ("LEVEL")

    def unlock_next_level(self, current):
        prev_lvl = ""
        for i in iter(self.all_level_data["DATA"]):
            for j in iter(i):
                if prev_lvl == current:
                    self.save[i[j]] = True
                    save_file = open(Constant.SAVE_FILE, "w+")
                    json.dump(self.save, save_file)
                    save_file.close()
                    return
                prev_lvl = i[j]

    def get_next_level(self, current):
        prev_lvl = ""
        for i in iter(self.all_level_data["DATA"]):
            for j in iter(i):
                if prev_lvl == current:
                    return (i[j])
                prev_lvl = i[j]
        return ("")
