##
## Player
##

import pygame
from . import Constant, Field

class Jeff:

    def __init__(self, pos):
        self.pos = pos
        self.skin = pygame.image.load(Constant.JEFF_PATH)
        self.power = 100.0
        self.p_skins = []
        self.is_jumping = False
        self.jump_speed = 0.0
        self.is_die = False
        self.jump_acceleration = 0.0
        for i in range(Constant.JEFF_P_MAX):
            self.p_skins.append(pygame.image.load(Constant.JEFF_P_PATH + str(i + 1) + ".png"))

    def is_dead(self):
        return (self.is_die)

    def get_pos(self):
        return (self.pos)

    def die(self):
        self.is_die = True
        print("I'm dead :'(")

    def obtain_energy(self):
        if self.power < 0:
            self.power = 0
        self.power += 20
        if self.power > 100:
            self.power = 100

    def jump(self, state):
        if self.power <= 0:
            state = False
        self.is_jumping = state
        if state and self.jump_speed == 0:
            self.jump_speed = Constant.BASE_JUMP_SPEED
            self.jump_acceleration = Constant.BASE_JUMP_ACCELERATION

    def update(self, time_since_last_frame, field):
        self.power -= time_since_last_frame * Constant.BASE_POWER_WASTE
        self.pos[1] -= self.jump_speed * (time_since_last_frame / 8)
        self.jump_speed *= pow(self.jump_acceleration, (time_since_last_frame / 8))
        if self.jump_speed < 1 and self.jump_speed > 0:
            self.jump_speed *= -1
            self.jump_acceleration = Constant.BASE_FALL_ACCELERATION
        if self.jump_speed < -50:
            self.jump_speed = -50
            self.jump_acceleration = 1
        bottom_height = field.get_height_at_point([self.pos[0], self.pos[1]])
        bottom_height_2 = field.get_height_at_point([self.pos[0] + 50, self.pos[1]])
        if bottom_height_2 < bottom_height:
            bottom_height = bottom_height_2
        if bottom_height < self.pos[1] + self.skin.get_height() :
            self.pos[1] = bottom_height - self.skin.get_height()
            self.jump_speed = 0
            self.jump_acceleration = 0
            if self.power <= 0:
                self.is_jumping = False
            if self.is_jumping:
                self.jump_speed = Constant.BASE_JUMP_SPEED
                self.jump_acceleration = Constant.BASE_JUMP_ACCELERATION
        if field.get_height_at_point([self.pos[0], self.pos[1]]) > self.pos[1] + self.skin.get_height() and self.jump_speed == 0:
            self.jump_speed = -1
            self.jump_acceleration = Constant.BASE_FALL_ACCELERATION

    def draw(self, screen):
        final_skin = pygame.Surface(self.skin.get_size())
        final_skin.blit(self.skin, [0, 0])
        power = 0
        while power * 20 < self.power:
            if self.power <= 10 and int(self.power) % 2:
                break;
            final_skin.blit(self.p_skins[power], [0, 0])
            power += 1
        screen.blit(final_skin, self.pos)

    def get_power(self):
        return (self.power)
