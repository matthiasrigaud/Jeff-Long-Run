##
## Event Manager for level editor
##

import pygame
import sys

from . import Field_editor

def manager(field):
    put_tile = False
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_ESCAPE):
            sys.exit()
        elif event.type == pygame.KEYDOWN and (event.dict["key"] == pygame.K_RIGHT or event.dict["key"] == pygame.K_d):
            field.change_right_move(True)
        elif event.type == pygame.KEYUP and (event.dict["key"] == pygame.K_RIGHT or event.dict["key"] == pygame.K_d):
            field.change_right_move(False)
        elif event.type == pygame.KEYDOWN and (event.dict["key"] == pygame.K_LEFT or event.dict["key"] == pygame.K_q):
            field.change_left_move(True)
        elif event.type == pygame.KEYUP and (event.dict["key"] == pygame.K_LEFT or event.dict["key"] == pygame.K_q):
            field.change_left_move(False)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.dict["button"] == 4:
            field.change_tile(1)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.dict["button"] == 5:
            field.change_tile(-1)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.dict["button"] == 1:
            field.put_tile()
