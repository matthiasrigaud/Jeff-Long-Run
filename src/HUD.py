##
## HUD
##

import pygame

from . import Field, Level_select

def draw_hud(screen, font, dead, field, level_select):
    if dead:
        text_surface = font.render("You're dead :'(", True, [255, 100, 100])
        screen.blit(text_surface, [500, 100])
        text_surface = font.render("Press R to Retry or M to join Menu", True, [255, 255, 255])
        screen.blit(text_surface, [370, 150])
    if field.is_level_ended():
        text_surface = font.render(field.get_level_name() + " done !", True, [100, 255, 100])
        screen.blit(text_surface, [500, 100])
        if level_select.get_next_level(field.get_level_file()) == "":
            text_surface = font.render("Press M to join Menu or N to got to next ...", True, [255, 255, 255])
            screen.blit(text_surface, [270, 150])
            text_surface = font.render("Err ... it's the last one.", True, [255, 255, 255])
            screen.blit(text_surface, [400, 200])
            text_surface = font.render("Maybe you can go out or something like that now : )", True, [255, 255, 255])
            screen.blit(text_surface, [230, 250])
            text_surface = font.render("PS: GG !", True, [255, 255, 255])
            screen.blit(text_surface, [550, 300])
        else:
            text_surface = font.render("Press M to join Menu or N to go to next level", True, [255, 255, 255])
            screen.blit(text_surface, [250, 150])
