##
## class to edite field
##

import pygame
import json
import sys
from . import Constant

class Field:

    def __init__(self, y):
        self.y = y
        self.x = 0
        self.right = False
        self.left = False
        self.ground = pygame.image.load(Constant.GROUND_PATH)
        self.level_data = []
        self.level_cursor = 0
        self.cur_tile = 0
        self.level_name = ""

    def change_right_move(self, right):
        self.right = right

    def change_left_move(self, left):
        self.left = left

    def change_tile(self, inc):
        self.cur_tile += inc
        if self.cur_tile < 0:
            self.cur_tile = len(self.level_data["TILES"]) - 1
        if self.cur_tile >= len(self.level_data["TILES"]):
            self.cur_tile = 0

    def put_tile(self):
        x_cursor = int(self.level_cursor) + int(pygame.mouse.get_pos()[0] / 50)
        y_cursor = int((self.y - pygame.mouse.get_pos()[1]) / 50)
        if y_cursor < 0:
            return
        if self.level_data["TILES"][self.cur_tile]["NAME"] == "End":
            y_cursor = 3
        while len(self.level_data["DATA"]) <= y_cursor:
            self.level_data["DATA"].append([])
        while len(self.level_data["DATA"][y_cursor]) <= x_cursor:
            self.level_data["DATA"][y_cursor].append(0)
        self.level_data["DATA"][y_cursor][x_cursor] = self.cur_tile
        self.save_level(self.level_name)

    def update(self):
        movement = 0
        if self.right:
            movement += 1
        if self.left:
            movement -= 1
        if self.level_cursor == 0 and movement < 0:
            movement = 0
        self.level_cursor += movement
        self.x -= movement * 50
        while self.x <= -self.ground.get_width():
            self.x += self.ground.get_width()
        while self.x > 0:
            self.x -= self.ground.get_width()

    def draw(self, screen):
        screen.blit(self.ground, [self.x, self.y])
        if self.x != 0:
            screen.blit(self.ground, [self.x + self.ground.get_width(), self.y])
        for j in range(len(self.level_data["DATA"])):
            for i in range(int(self.level_cursor), int(self.level_cursor) + int(Constant.WIN_WIDTH / 50) + 1):
                if i >= len(self.level_data["DATA"][j]):
                    break
                if len(self.level_data["TILES"][self.level_data["DATA"][j][i]]["IMG"]) != 0:
                    screen.blit(self.level_data["TILES"][self.level_data["DATA"][j][i]]["SURFACE"], [(i - self.level_cursor) * 50, self.y - 50 * (j + 1)])
        if self.level_data["TILES"][self.cur_tile]["NAME"] == "End":
            screen.blit(self.level_data["TILES"][self.cur_tile]["SURFACE"], [pygame.mouse.get_pos()[0], self.y - 200])
        elif len(self.level_data["TILES"][self.cur_tile]["IMG"]) != 0:
            screen.blit(self.level_data["TILES"][self.cur_tile]["SURFACE"], pygame.mouse.get_pos())

    def load_level(self, file_name):
        try:
            level_file = open(file_name)
        except IOError:
            print("file don't exist")
            sys.exit()
        self.level_name = file_name
        self.level_data = json.load(level_file)
        level_file.close()
        self.level_cursor = 0.0
        print(self.level_data["MAP_NAME"])
        for i in iter(self.level_data["TILES"]):
            if len(i["IMG"]) != 0:
                i["SURFACE"] = pygame.image.load(Constant.ASSETS_DIR + "/" + i["IMG"])

    def save_level(self, file_name):
        level_file = open(file_name, "w+")
        for i in iter(self.level_data["TILES"]):
            if len(i["IMG"]) != 0:
                del i["SURFACE"]
        json.dump(self.level_data, level_file)
        for i in iter(self.level_data["TILES"]):
            if len(i["IMG"]) != 0:
                i["SURFACE"] = pygame.image.load(Constant.ASSETS_DIR + "/" + i["IMG"])
        level_file.close()
