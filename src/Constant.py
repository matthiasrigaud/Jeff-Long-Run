#### CONSTANT GLOBAL VALUES ####

ASSETS_DIR      = "assets"
LEVELS_DIR      = "levels"

WIN_WIDTH       = 1280
WIN_HEIGHT      = 720
WIN_TITLE       = "Jeff Long Run"

GROUND_PATH     = ASSETS_DIR + "/field.png"
GROUND_HEIGHT   = 4 * WIN_HEIGHT / 5

JEFF_PATH       = ASSETS_DIR + "/jeff.png"
JEFF_P_PATH     = ASSETS_DIR + "/p"
JEFF_P_MAX      = 5

FONT_PATH       = ASSETS_DIR + "/JUST DO GOOD.ttf"

LEVEL_FILE      = LEVELS_DIR + "/game.json"

SAVE_FILE       = LEVELS_DIR + "/save.json"

##
## speed in pix/millisecond
## power in percent/millisecond
##

BASE_SPEED = 1

BASE_POWER_WASTE        = 0.02

BASE_JUMP_SPEED         = 10.0
BASE_JUMP_ACCELERATION  = 0.9
BASE_FALL_ACCELERATION  = 1.2
