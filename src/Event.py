##
## Event Manager
##

import pygame
import sys
from . import Jeff, Constant

def manager(jeff, field, frame, level_select):
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_ESCAPE):
            sys.exit()
        elif event.type == pygame.KEYDOWN and jeff.is_dead() and event.dict["key"] == pygame.K_r:
            jeff = Jeff.Jeff([300, Constant.GROUND_HEIGHT - 50])
            field.reload_level()
        elif event.type == pygame.KEYDOWN and (jeff.is_dead() or field.is_level_ended()) and event.dict["key"] == pygame.K_m:
            jeff = Jeff.Jeff([300, Constant.GROUND_HEIGHT - 50])
            frame = "MENU"
        elif event.type == pygame.KEYDOWN and field.is_level_ended() and event.dict["key"] == pygame.K_n:
            next_level = level_select.get_next_level(field.get_level_file())
            if next_level != "":
                jeff = Jeff.Jeff([300, Constant.GROUND_HEIGHT - 50])
                field.load_level(next_level)
        elif event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_SPACE:
            jeff.jump(True)
        elif event.type == pygame.KEYUP and event.dict["key"] == pygame.K_SPACE:
            jeff.jump(False)
    return jeff, frame
