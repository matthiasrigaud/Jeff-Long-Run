##
## class to manage field
##

import pygame
import json
from . import Constant, Jeff, Level_select

class Field:

    def __init__(self, y):
        self.y = y
        self.x = 0
        self.ground = pygame.image.load(Constant.GROUND_PATH)
        self.level_data = []
        self.last_level = ""
        self.lvl_end = False
        self.level_cursor = 0.0

    def is_level_ended(self):
        return (self.lvl_end)

    def get_level_name(self):
        return (self.level_data["MAP_NAME"])

    def get_level_file(self):
        return (self.last_level)

    def check_collisions(self, jeff, level_select):
        for j in range(len(self.level_data["DATA"])):
            # if self.y - 50 * (j + 1) < jeff.get_pos()[1]:
            #     break
            for i in range(int(self.level_cursor), int(self.level_cursor) + int(Constant.WIN_WIDTH / 50) + 1):
                if i >= len(self.level_data["DATA"][j]) or (i - self.level_cursor) * 50 > jeff.get_pos()[0] + 50:
                    break
                if self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"] == "End":
                    self.lvl_end = True
                    level_select.unlock_next_level(self.last_level)
                if pygame.Rect([jeff.get_pos()[0] + 2, jeff.get_pos()[1] - 2],
                               [46, 46]).colliderect(pygame.Rect([(i - self.level_cursor) * 50, self.y - 50 * (j + 1)], [50, 50])) and \
                               self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"] == "Bloc":
                    print(self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"])
                    jeff.die()
                elif pygame.Rect([jeff.get_pos()[0] + 10, jeff.get_pos()[1] - 10],
                                 [30, 30]).colliderect(pygame.Rect([(i - self.level_cursor) * 50, self.y - 50 * (j + 1)], [50, 50])):
                    if self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"] == "Pic":
                        print(self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"])
                        jeff.die()
                    elif self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"] == "Energy":
                        self.level_data["DATA"][j][i] = 0
                        jeff.obtain_energy()


    def update(self, time_since_last_frame):
        estimated_distance = time_since_last_frame * Constant.BASE_SPEED
        self.level_cursor += estimated_distance / 50.0
        self.x -= estimated_distance
        while self.x <= -self.ground.get_width():
            self.x += self.ground.get_width()
        return (estimated_distance)

    def get_height_at_point(self, pos):
        higher = self.y
        for j in range(len(self.level_data["DATA"])):
            for i in range(int(self.level_cursor), int(self.level_cursor) + int(Constant.WIN_WIDTH / 50) + 1):
                if i >= len(self.level_data["DATA"][j]) or (i - self.level_cursor) * 50 > pos[0]:
                    break
                if self.level_data["TILES"][self.level_data["DATA"][j][i]]["NAME"] == "Bloc" \
                   and self.y - 50 * (j + 1) < higher \
                   and self.y - 50 * (j + 1) > pos[1] \
                   and (i - self.level_cursor) * 50 < pos[0] \
                   and (i - self.level_cursor + 1) * 50 > pos[0]:
                    higher = self.y - 50 * (j + 1)
                    break
        return (higher)

    def draw(self, screen):
        screen.blit(self.ground, [self.x, self.y])
        if self.x != 0:
            screen.blit(self.ground, [self.x + self.ground.get_width(), self.y])
        for j in range(len(self.level_data["DATA"])):
            for i in range(int(self.level_cursor), int(self.level_cursor) + int(Constant.WIN_WIDTH / 50) + 1):
                if i >= len(self.level_data["DATA"][j]):
                    break
                if len(self.level_data["TILES"][self.level_data["DATA"][j][i]]["IMG"]) != 0:
                    screen.blit(self.level_data["TILES"][self.level_data["DATA"][j][i]]["SURFACE"], [(i - self.level_cursor) * 50, self.y - 50 * (j + 1)])

    def load_level(self, file_name):
        self.last_level = file_name
        self.lvl_end = False
        level_file = open(Constant.LEVELS_DIR + "/" + file_name)
        self.level_data = json.load(level_file)
        level_file.close()
        self.level_cursor = 0.0
        print(self.level_data["MAP_NAME"])
        for i in iter(self.level_data["TILES"]):
            if len(i["IMG"]) != 0:
                i["SURFACE"] = pygame.image.load(Constant.ASSETS_DIR + "/" + i["IMG"])

    def reload_level(self):
        self.load_level(self.last_level)
