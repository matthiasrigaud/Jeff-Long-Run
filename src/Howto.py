##
## How to play
##

import pygame

from . import User_interface, Constant

class How_to_play:

    def __init__(self):
        self.buttons = {}
        self.buttons["MENU"] = User_interface.Button(Constant.FONT_PATH, [350, 80], [20, 620], 2,
                                                     [[157, 180, 195], [187, 210, 225], [170, 190, 205]],
                                                     {"TXT": "<< Back to Menu",
                                                      "SIZE": 28,
                                                      "COLOR": [[250, 250, 250], [200, 200, 200], [150, 150, 150]]})

    def draw(self, screen, font):
        screen.fill([157, 180, 195])
        for i in iter(self.buttons):
            self.buttons[i].draw(screen)
        text_surface = font.render("HOW TO PLAY", True, [150, 200, 255])
        screen.blit(text_surface, [550, 50])
        text_surface = font.render("Help Jeff, the tiny squared robot, to travel throughout the levels :", True, [255, 255, 255])
        screen.blit(text_surface, [int((Constant.WIN_WIDTH - text_surface.get_size()[0]) / 2), 200])
        text_surface = font.render("Jump with space to prevent obstacles", True, [255, 255, 255])
        screen.blit(text_surface, [int((Constant.WIN_WIDTH - text_surface.get_size()[0]) / 2), 300])
        text_surface = font.render("and join safely the end of each level.", True, [255, 255, 255])
        screen.blit(text_surface, [int((Constant.WIN_WIDTH - text_surface.get_size()[0]) / 2), 350])
        text_surface = font.render("Don't forget to take care of your battery !", True, [255, 255, 255])
        screen.blit(text_surface, [int((Constant.WIN_WIDTH - text_surface.get_size()[0]) / 2), 450])

    def update(self):
        for i in iter(self.buttons):
            if self.buttons[i].is_overflew() and pygame.mouse.get_pressed()[0]:
                self.buttons[i].set_state(0)
                return (i)
            elif self.buttons[i].is_overflew():
                self.buttons[i].set_state(1)
            else:
                self.buttons[i].set_state(2)
        return ("HOWTO")
