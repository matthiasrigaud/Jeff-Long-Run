#!/usr/bin/env python3

##
## Main File
##

import pygame
import sys
import time

sys.path.append('.')

from src import Constant, Event_editor, Field_editor

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("Please give file to edit (must exist and be a valid level file)")
        sys.exit()
    pygame.init()
    screen = pygame.display.set_mode((Constant.WIN_WIDTH, Constant.WIN_HEIGHT))
    pygame.display.set_caption(Constant.WIN_TITLE + " - editor")
    screen.fill([157, 180, 195])
    distance = 0
    field = Field_editor.Field(Constant.GROUND_HEIGHT)
    field.load_level(sys.argv[1])

    while 1:

        Event_editor.manager(field)

        field.update()
        screen.fill([157, 180, 195])
        field.draw(screen)
        pygame.display.flip()
        time.sleep(0.01)
