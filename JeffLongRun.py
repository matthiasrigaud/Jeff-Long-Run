#!/usr/bin/env python3

##
## Main File
##

import pygame
import sys

sys.path.append('.')

from src import Constant, Event, Field, Jeff, HUD, Menu, Level_select, Howto

if __name__ == '__main__':

    pygame.init()
    screen = pygame.display.set_mode((Constant.WIN_WIDTH, Constant.WIN_HEIGHT))
    pygame.display.set_caption(Constant.WIN_TITLE)
    screen.fill([157, 180, 195])
    hud_font = pygame.font.Font(Constant.FONT_PATH, 30)
    field = Field.Field(Constant.GROUND_HEIGHT)
    menu = Menu.Menu()
    howto = Howto.How_to_play()
    level_select = Level_select.Level_select()
    frame = "MENU"
    jeff = Jeff.Jeff([300, Constant.GROUND_HEIGHT - 50])
    old_time = 0

    while 1:

        cur_time = pygame.time.get_ticks()

        time_since_last_frame = cur_time - old_time
        old_time = cur_time

        jeff, frame = Event.manager(jeff, field, frame, level_select)

        if frame == "LEVEL":
            back = level_select.update()
            level_select.draw(screen)
            if back == "MENU":
                frame = "MENU"
            elif back == "LEVEL":
                frame = "LEVEL"
            else:
                frame = "PLAY"
                field.load_level(back)
        elif frame == "PLAY":
            if not jeff.is_dead() and not field.is_level_ended():
                field.update(time_since_last_frame)
                field.check_collisions(jeff, level_select)
                if not jeff.is_dead() and not field.is_level_ended():
                    jeff.update(time_since_last_frame, field)
            screen.fill([157, 180, 195])
            field.draw(screen)
            jeff.draw(screen)
            HUD.draw_hud(screen, hud_font, jeff.is_dead(), field, level_select)
        elif frame == "MENU":
            frame = menu.update()
            menu.draw(screen)
        elif frame == "HOWTO":
            frame = howto.update()
            howto.draw(screen, hud_font)
        elif frame == "QUIT":
            sys.exit()
        pygame.display.flip()
