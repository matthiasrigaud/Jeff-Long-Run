![Jeff Long Run](https://gitlab.com/matthiasrigaud/Jeff-Long-Run/raw/master/assets/menu_back.png)

## Instructions

Use Python3 and Pygame to work, so Linux users must install them (use your package manager for python, i. e. `pacman -S python3`, and pip to install pygame `sudo pip install pygame`).

A [stand-alone executable for Windows](https://matoux42.itch.io/jeff-long-run) is available.

## Story

Jeff, the tiny squared robot, want to travel around the world. Help him to realize his dream !

Unfortunately, Jeff forgot he can't do anything else than go forward when he's running out of power. It will be harder than expected ... Good Luck !

## Controls

* Use space to jump

## Level editor

A basic level editor is available on the repository. It takes on parameter an existing and valid level file and allow to edit it.
`Usage : ./LevelEditor.py path/to/level_file.json`

You can switch blocs with mouse wheel, put them with left click and move on the map with left/right arrow keys.

## LD39

This game was made for the LD39 Compo : [LD39 Profile](https://ldjam.com/events/ludum-dare/39/jeff-long-run).